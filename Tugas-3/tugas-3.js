// soal no 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var a = kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1);
var b = kataKeempat.toUpperCase();
console.log(kataPertama.concat(a).concat(kataKetiga).concat(b));

// soal no 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";
console.log(kataPertama+kataKedua+kataKetiga+kataKeempat);

// soal no 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24);  
var kataKelima = kalimat.substring(25, 31);  

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// soal no4
var nilai;

nilai >= 80 indeksnya A
nilai >= 70 dan nilai < 80 indeksnya B
nilai >= 60 dan nilai < 70 indeksnya c
nilai >= 50 dan nilai < 60 indeksnya D
nilai < 50 indeksnya E

// soal no5
var tanggal = 21;
var bulan = 1;
var tahun = 2000;
console.log(tanggal + " " + bulan + " " + tahun);