// soal 1
class Animal {
    constructor(name){
        this.animalname = name;
        this._animallegs = legs;
        this._animalcoldblooded = cold_blooded;
    }
    get animalname() {
        return this.animalname;
    }
    set animalname(x) {
        this.animalname = x;
    }
}

var sheep = nem Animal("shaun")

console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

// release 1
class Model extends Animal{
    constructor(name){
        super(name);
        this._name = name;
        this.suara = "Auoo";
        this.sound = "hop hop";
    }
    get name(){
        return this.name;
    }
    setname(x){
        return (this.name=x)
    }
    yell(){
        return console.log(this.suara)
    }
    jump(){
        return console.log(this.sound)
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell()

var kodok = new Frog("buduk")
kodok.jump()

// soal 2
class Clock{
    constructor({template}){
        this._template= template
        this.timer
    }
    render(){
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
        console.log(this._template)
    }
    stop(){
        clearInterval(this.timer);
    };
    start(){
        render();
        timer= setInterval(render, 1000);
    };
  }
  
let clock = new Clock({template: 'h:m:s'});
clock.start(); //bingunggggg :()