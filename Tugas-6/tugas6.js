// soal 1
// Luas
const luasLingkaran =(a, b)=>{
    let hasil = a * (b * b);
    return hasil;
}
console.log(luasLingkaran(1.12, 7))
// Keliling
const kelilingLingkaran=(c, d)=>{
    let jumlah= c * d;
    return jumlah;
}
console.log(kelilingLingkaran(1.12, 7))

// soal 2
let kalimat="saya adalah seorang frontend developer";
let theString= '$(Kalimat)'
console.log(theString)

//soal 3
const newFunction= function literal(firstName, lastName){
    return {
        firstName, lastName,
        fullName: function(){
            console.log(firstName + " " + lastName)
        }
    }
}

newFunction("William", "Imoh").fullname()

//soal 4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}